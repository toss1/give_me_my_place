package com.jh.appresult;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    public wHandler whandler = new wHandler();
    public Button button1;
    public Button button2;
    public TextView textview1;
    public TextView textview2;
    public TextView ToastTextview;
    public String ToastMsg = "";
    public int cnt = 0;
    public int cnt2 = 0;
    public int w1sw = 0;
    public int w2sw = 0;
    public int w1sw1 = 0;
    public int w2sw2 = 0;
    public Timer timer;
    public Map<String, String> w1;
    public Map<String, String> w2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button1 = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);
        textview1 = (TextView) findViewById(R.id.textView3);
        textview2 = (TextView) findViewById(R.id.textView4);
        ToastTextview = (TextView) findViewById(R.id.textView5);

        wRequest wreq = new wRequest();
        wreq.start();


        button1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (button1.getText() == "0") {
                    textview1.setBackgroundColor(Color.parseColor("#CA0000"));
                    textview1.setText(" ");
                    button1.setText("1");
                } else {
                    textview1.setBackgroundColor(Color.parseColor("#FFEB3B"));
                    button1.setText("0");
                    textview1.setText(" ");
                    w1sw = 0;
                    cnt = 0;
                }
            }
        });

        button2.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (button2.getText() == "0") {
                    textview2.setBackgroundColor(Color.parseColor("#CA0000"));
                    button2.setText("1");
                    textview2.setText(" ");
                    w2sw = 0;
                } else {
                    textview2.setBackgroundColor(Color.parseColor("#FFEB3B"));
                    button2.setText("0");
                    textview2.setText(" ");
                    w2sw = 0;
                    cnt2 = 0;
                }
            }
        });

    }

    //무게 UI처리 w1핸들러
    class wHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            w1 = Parser(bundle.getString("w1"));
            w2 = Parser(bundle.getString("w2"));

            //1번 사용자 등록X
            if (button1.getText().equals("0")) {
                textview1.setText(" ");
                if (Integer.parseInt(w1.get("con")) > 3)
                    ToastMsg += "1번 자리를 확인해주세요.\n";
            }


            //1번 사용자 등록O
            else if (button1.getText().equals("1")) {
                //무게 > 30(좌석에 사람 존재)
                if (Integer.parseInt(w1.get("con")) > 30) {
                    textview1.setText(" ");
                    cnt = 0;
                    w1sw = 0;
                    w1sw1 = 0;
                }

                // 무게 < 30 (좌석에 물건에 있는 것으로 인식)
                else if (Integer.parseInt(w1.get("con")) < 30) {
                    if (w1sw == 1) {
                        textview1.setText("사용 종료");
                        if(!ToastMsg.contains("1번 자리를 확인해주세요\n"))
                            ToastMsg += "1번 자리를 확인해주세요\n";
                    } else if (w1sw == 0) {
                        ToastMsg += "1번 자리에 짐이 있습니다.\n";
                        if(timep(cnt).equals("00:02"))
                            textview1.setText("사용 종료");
                        else if(timep(cnt).equals("00:01"))
                            ToastMsg += "1번 자리가 10분 남았습니다.\n";
                        else
                            textview1.setText(timep(cnt));
                        timer = new Timer(true);
                        TimerTask tt = new TimerTask() {
                            @Override
                            public void run() {
                                if(Integer.parseInt(w1.get("con")) > 30){
                                    textview1.setText(" ");
                                    cnt = 0;
                                    w1sw = 0;
                                    w1sw1 = 0;
                                    timer.cancel();
                                }
                                if (w1sw == 1) {
                                    timer.cancel();
                                } else if (textview1.getText().equals("01:00")) {
                                    w1sw1 = 1;
                                    w1sw = 1;
                                    cnt = 0;
                                    timer.cancel();
                                } else if (textview1.getText().equals("00:50") && w1sw1 == 0) {
                                    cnt++;
                                    w1sw1 = 1;
                                } else {
                                    cnt++;
                                }
                            }
                            @Override
                            public boolean cancel() {
                                return super.cancel();
                            }
                        };
                        timer.schedule(tt, 100, 1000);
                    }
                }
            }


            //2번 사용자 등록X
            if (button2.getText().equals("0")) {
                textview2.setText(" ");
                if (Integer.parseInt(w2.get("con")) > 3)
                    ToastMsg += "2번 자리를 확인해주세요.\n";
            }
            //2번 사용자 등록O
            else if (button2.getText().equals("1")) {

                //무게 > 30(좌석에 사람 존재)
                if (Integer.parseInt(w2.get("con")) > 30) {
                    textview2.setText(" ");
                    cnt2 = 0;
                    w2sw = 0;
                    w2sw2 = 0;
                }
                //무게 < 30 (좌석에 물건에 있는 것으로 인식)
                else if (Integer.parseInt(w2.get("con")) < 30) {
                    if (w2sw == 1) {
                        textview2.setText("사용 종료");
                        if(!ToastMsg.contains("2번 자리를 확인해주세요\n"))
                            ToastMsg += "2번 자리를 확인해주세요\n";
                    } else if (w2sw == 0) {
                        ToastMsg += "2번 자리에 짐이 있습니다.\n";
                        if(timep(cnt2).equals("00:02"))
                            textview2.setText("사용 종료");
                        else if(timep(cnt2).equals("00:01"))
                            ToastMsg += "2번 자리가 10분 남았습니다.\n";
                        else
                            textview2.setText(timep(cnt2));

                        timer = new Timer(true);
                        TimerTask tt = new TimerTask() {
                            @Override
                            public void run() {
                                if(Integer.parseInt(w2.get("con")) > 30){
                                    textview2.setText(" ");
                                    cnt2 = 0;
                                    w2sw = 0;
                                    w2sw2 = 0;
                                    timer.cancel();
                                }
                                if (w2sw == 1) {
                                    timer.cancel();
                                } else if (textview2.getText().equals("01:00")) {
                                    w2sw2 = 1;
                                    w2sw = 1;
                                    cnt2 = 0;
                                    timer.cancel();
                                } else if (textview2.getText().equals("00:50") && w2sw2 == 0) {
                                    cnt2++;
                                    w2sw2 = 1;
                                } else {
                                    cnt2++;
                                }
                            }

                            @Override
                            public boolean cancel() {
                                return super.cancel();
                            }
                        };
                        timer.schedule(tt, 100, 1000);
                    }
                }
            }

            if (ToastMsg != "") {
                ToastTextview.setVisibility(View.VISIBLE);
                ToastTextview.setText(ToastMsg);
            } else
                ToastTextview.setVisibility(View.INVISIBLE);
            ToastMsg = "";
        }
    }

    class wRequest extends Thread {
        public wRequest() {
        }

        @Override
        public void run() {
            while (true) {
                try {
                    //w1데이터 가져옴
                    URL url = new URL("http://203.250.148.89:7579/Mobius/Team_2/user1/la");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.setDoOutput(false);

                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("X-M2M-RI", "12345");
                    conn.setRequestProperty("X-M2M-Origin", "SSJmmDDV65D");
                    conn.connect();

                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    Bundle bundle = new Bundle();
                    bundle.putString("w1", in.readLine());


                    conn.disconnect();

                    //w2데이터
                    url = new URL("http://203.250.148.89:7579/Mobius/Team_2/user2/la");
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.setDoOutput(false);

                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("X-M2M-RI", "12345");
                    conn.setRequestProperty("X-M2M-Origin", "SSJmmDDV65D");
                    conn.connect();

                    in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    Message msg = whandler.obtainMessage();
                    bundle.putString("w2", in.readLine());
                    msg.setData(bundle);
                    whandler.sendMessage(msg);

                    conn.disconnect();

                    Thread.sleep(2000);

                } catch (Exception e) {
                    System.out.println("w1 error : " + e);
                }
            }
        }
    }

    public Map<String, String> Parser(String str) {
        String data = str.replace("{", "");
        data = data.replace("}", "");
        data = data.replace("\"m2m:cin\"", "");
        String[] array = data.split(",");


        Map<String, String> cin = new HashMap<>();
        String[] arr;
        for (int i = 0; i < array.length; i++) {
            arr = array[i].split(":");
            arr[0] = arr[0].replace("\"", "");
            arr[1] = arr[1].replace("\"", "");
            cin.put(arr[0], arr[1]);
        }
        return cin;
    }

    public String timep(int cnt) {
        String time, h, m;
        h = String.valueOf(cnt / 3600);
        cnt %= 3600;
        m = String.valueOf(cnt / 60);
        if (Integer.parseInt(h) < 10)
            h = "0" + h;
        if (Integer.parseInt(m) < 10)
            m = "0" + m;

        time = h + ":" + m;
        return time;
    }


}