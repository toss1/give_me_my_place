#include <WiFiClient.h>         //WIFI shield
#include <ESP8266WiFi.h>  //ESP8266
#include <PubSubClient.h>
 
const char *ssid = "InfoSec";
const char *password = "wjdqhqhgh16";
const char* mqtt_server = "203.250.148.89"; //mqtt_server_IP
 
WiFiClient espClient;
PubSubClient client(espClient);
 
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("bound7")) { //change to ClientID
      Serial.println("connected");
       
      // ... and resubscribe
      client.subscribe("Topic");
 
      // Once connected, publish an announcement...
      client.publish("Topic", "test msg");
       
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
    }
  }
}
 
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String msg = "";
  for (int i = 0; i < length; i++) {
    msg +=(char)payload[i];
  }
  Serial.print(msg);
  Serial.println();
}
 
void setup() {
  Serial.begin(115200);
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  client.setServer(mqtt_server, 7579);
  client.setCallback(callback);
   
}
 
void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
   
}
