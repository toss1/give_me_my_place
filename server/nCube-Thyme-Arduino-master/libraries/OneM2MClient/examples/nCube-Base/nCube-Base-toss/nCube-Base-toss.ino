#include <Arduino.h>
#include <SPI.h>
#include <FlashAsEEPROM.h>

/**
   Copyright (c) 2017, OCEAN
   All rights reserved.
   Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
   1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3. The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.
   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <ArduinoJson.h>

#include "OneM2MClient.h"

#include "HX711.h" //HX711로드셀 라이브러리 불러오기
#define calibration_factor -7050.0 // 로드셀 초기값을 설정해줍니다. 이렇게 해주어야 처음에 작동시에 0점을 맞추는 것이라고 생각하시면 됩니다.
#define DOUT  3 //엠프 데이터 아웃 핀 
#define CLK  2  //엠프 클락  

HX711 HX711Sensor;

HX711 scale(DOUT, CLK); //엠프 핀 선언 

const String AE_ID = "weight";
const String MQTT_BROKER_IP = "203.253.128.161"; // 모비우스가 설치되어 동작하는 IP 입력
const uint16_t MQTT_BROKER_PORT = 1883;
OneM2MClient nCube(MQTT_BROKER_IP, MQTT_BROKER_PORT, AE_ID); // AE-ID

unsigned long req_previousMillis = 0;
const long req_interval = 10000; //센싱주기 60초로 설정

unsigned long HX_sensing_previousMillis = 0;
const long HX_sensing_interval = (1000 * 60); weight 센서를 60초 주기로 센싱하도록 설정

short action_flag = 0;
short sensing_flag = 0;
short control_flag = 0;

String noti_con = "";

char body_buff[400];  //for inputting data to publish
char req_id[10];       //for generating random number for request packet id

String resp_rqi = "";

String state = "init";

String curValue = "";

/*************************** Sketch Code ************************************/


void setup() {


    //while (!Serial);
    Serial.begin(115200);
    Serial.println("Weight detection start");  
    
    delay(100);

    nCube.begin();

    nCube.setCallback(resp_callback, noti_callback);

    buildResource();

    state = "create_ae";

    HX711Sensor.init();
    HX711Sensor.setCallback(HX711Sensor_upload_callback);

    scale.set_scale(calibration_factor);  
    scale.tare();   
}

void loop() {
    if (nCube.chkConnect()) {
        nCube.chkInitProvision();

        unsigned long currentMillis = millis();

        if (currentMillis - req_previousMillis >= req_interval) {
            req_previousMillis = currentMillis;
            publisher();
        }
        else if (currentMillis - HX_sensing_previousMillis >= HX_sensing_interval) {
            HX_sensing_previousMillis = currentMillis;

            if (state == "create_cin") {
                // guide: in here generate sensing data
                // if get sensing data directly, assign curValue sensing data and set sensing_flag to 1
                // if request sensing data to sensor, set sensing_flag to 0, in other code of receiving sensing data, assign curValue sensing data and set sensing_flag to 1
                
                HX711Sensor.requestData();
                sensing_flag = 0;
            }
        }
        else {
            if (state == "create_cin") {
                if (sensing_flag == 1) {
                    rand_str(req_id, 8);
                    nCube.createCin(req_id, (nCube.resource[1].to + "/" + nCube.resource[1].rn), curValue);
                    sensing_flag = 0;
                }

                if (control_flag == 1) {
                    control_flag = 0;
                    // guide: in here control action code along to noti_con

                    Serial.print("weight: ");
                    Serial.print(scale.get_units(), 1);   
                    Serial.print(" lbs"); //단위
                    Serial.println(); 
                    delay(1000);


                    String resp_body = "";
                    resp_body += "{\"rsc\":\"2000\",\"to\":\"\",\"fr\":\"" + nCube.getAeid() + "\",\"pc\":\"\",\"rqi\":\"" + resp_rqi + "\"}";
                    resp_body.toCharArray(body_buff, resp_body.length() + 1);
                    nCube.response(body_buff);
                }
                HX711Sensor.chkCO2Data();
            }
        }
    }
    else {
        nCube.chkInitProvision2();
    }
}
