# 아두이노 시리얼 모니터에 출력된 로드셀 무게 데이터를 읽어와
# 서버로 전송해주는 파이썬 코드이다.

import serial           # 시리얼 모니터 값을 읽어오기 위하여 serial 모듈 import
import requests         # 서버와 통신하기 위하여 requests 모듈 import
# import time             # 딜레이를 주기 위해 필요하지만 본 코드에서는 사용되지 않음.

print('serial ' + serial.__version__)           #시리얼 버전 출력

# Set a PORT Number & baud rate 
PORT = 'COM5'                                   #시리얼 포트 지정 -> 아두이노가 연결된 시리얼 포트로 지정해줌
BaudRate = 115200                               #BaudRate 지정 -> 아두이노와 통일

ARD= serial.Serial(PORT,BaudRate) 

def Decode(A):      # 사용되지 않음
    A = A.decode() 
    A = str(A) 
    print(A)
    # if A[0]=='w':  #첫문자 검사
    #     if (len(A)>=10): #문자열 갯수 검사 
    #         Ard1=int(A[8:]) 
    #         # Ard2=int(A[5:9]) 
    #         result= [Ard1] 
    #         return result 
    #     else : 
    #         print ("Error_lack of number _ %d" %len(A)) 
    #         return False 
    # else : 
    #     print ("Error_Wrong Signal") 
    #     return False 

def Ardread(): # return list [Ard1,Ard2]        # 시리얼모니터 값을 한줄씩 읽어온 값 LINE을 반환
    if ARD.readable(): 
        LINE = ARD.readline() 
        # code=Decode(LINE) 
        print(LINE) 
        type(LINE)
        return LINE
    else : 
        print("읽기 실패 from _Ardread_") 

def server_post(weight):                                                # 데이터를 서버에 보내는 함수

    data = "{\"m2m:cin\": {\"con\": \"" + str(weight) + "\" }}"         # 데이터 형식은 postman으로 확인하여
    # data = "{\"m2m:cin\": {\"con\": \"149\" }}"                       # 형식을 맞춰줌
    headers = {'Accept': 'application/json',
    'X-M2M-RI':'12345',
    'X-M2M-Origin':'SSJmmDDV65D',
    'Postman-Token':'a4e39dae-57b3-4c5a-91ff-e162dc6f25b4',
    'Content-Type':'application/vnd.onem2m-res+json; ty=4',
    'Host':'203.250.148.89:7579',
    'User-Agent':'PostmanRuntime/7.26.8',
    'Connection':'keep-alive',
    'Content-Length':str(len(data))
    }

    response = requests.post('http://203.250.148.89:7579/Mobius/Team_2/user',headers=headers, data=data)    # 지정된 서버에 request 보냄

    print("data = ",data)               # data와 header 값이 정삭적으로 들어갔는지 확인하기 위하여
    print("headers = ",headers)         # 출력해주는 부분

    print(response.text)                # 응답을 출력. 201이면 정상.
   
while True:                             # 계속 반복
    data = Ardread()                    # 시리얼모니터에서 읽어온 값을 data에 저장
    server_post(data)                   # server_post 함수를 호출하여 data를 서버로 전송
    # time.sleep(5)