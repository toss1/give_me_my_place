#include <ESP8266WiFi.h>
#include <WiFiClient.h>

#include "HX711.h" //HX711로드셀 라이브러리 불러오기
#define calibration_factor -7050.0 // 로드셀 초기값을 설정해줍니다. 이렇게 해주어야 처음에 작동시에 0점을 맞추는 것이라고 생각하시면 됩니다.
#define DOUT  3 //엠프 데이터 아웃 핀 
#define CLK  2  //엠프 클락  
HX711 scale(DOUT, CLK); //엠프 핀 선언 


const char *ssid = "InfoSec";            // 와이파이 정보
const char *password = "wjdqhqhgh16";

const char* host = "203.250.148.89";     // 서버 주소




void setup() {
  Serial.begin(115200);
  delay(1000);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");    // 와이파이 연결 정보
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
/*
  Serial.print("Start time : ");
  unsigned long time = millis();
  Serial.println(time/1000);
*/
  Serial.println("Weight detection start");  
  scale.set_scale(calibration_factor);  
  scale.tare();   
  
}

int value = 0;
//String con;

void loop() {
  delay(5000);
  ++value;

  Serial.print("connecting to ");
  Serial.println(host);
  
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 7579;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  
/*
  Serial.print("Time : ");
  unsigned long time = millis();
  unsigned long timesec = time/1000;
  Serial.println(timesec);
  con.concat(timesec);
*/
  Serial.print("weight: ");                                // 무게 측정하여 시리얼모니터에 출력
  float weight = scale.get_units();
//  float weight = 13.2;
  Serial.print(weight);   
  Serial.println(" lbs"); //단위
//  con.concat(weight);



  // We now create a URI for the request
  String url = "/Mobius/Team_2/user";
//  url += streamId;
//  url += "?private_key=";
//  url += privateKey;
//  url += "&value=";
//  url += value;
  
  Serial.print("Requesting URL: ");
  Serial.println(url);
//  String PostData1 = "{\"m2m:cin\": {\"con\": \"";
//  String PostData2 = "\"}}";
//  String PostData = PostData1 + con + PostData2;
  String PostData = "{\"m2m:cin\": {\"con\": \"129\" }}";
  // This will send the request to the server
  client.println("POST /Mobius/Team_2/user HTTP/1.1");
  client.println("Accept: application/json");
  client.println("X-M2M-RI: 12345");
  client.println("X-M2M-Origin: SSJmmDDV65D");
  client.println("Postman-Token: a4e39dae-57b3-4c5a-91ff-e162dc6f25b4");
  client.println("Content-Type: application/vnd.onem2m-res+json; ty=4");
  client.println("Host: 203.250.148.89:7579");
  client.println("User-Agent: PostmanRuntime/7.26.8");
  client.println("Connection: keep-alive");
  client.print("Content-Length: ");
  client.println(PostData.length());
  client.println();
  client.println(PostData);
//  int timeout = millis() + 5000;
//  while (client.available() == 0) {
//    if (timeout - millis() < 0) {
//      Serial.println(">>> Client Timeout !");
//      client.stop();
//      return;
//    }
//  }
  
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  
  Serial.println();
  Serial.println("closing connection");
  return;
}
