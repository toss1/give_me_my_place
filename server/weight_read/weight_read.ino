#include "HX711.h" //HX711로드셀 라이브러리 불러오기
float calibration_factor = -30000; // 로드셀 초기값을 설정
#define DOUT  3 //엠프 데이터 아웃 핀 
#define CLK  2  //엠프 클락  
HX711 scale(DOUT, CLK); //엠프 핀 선언 

void setup() {
  Serial.begin(115200);  // 보드레이트를 지정해준다.

//  connect_wifi();
//  Serial.println("Weight detection start");  
  scale.set_scale(calibration_factor);  
  scale.tare();   
}

void loop() {
  Serial.print("weight: ");
  Serial.print(scale.get_units(), 1);   // 로드셀에서 읽어온 무게값
  Serial.println();
  //시리얼모니터에 출력되는 부분 (서버에 전송되는 부분)
  
//  Serial.print(" lbs"); //단위


//  server_post();
  
  delay(5000);    // 5초 딜레이. 루프는 5초에 한번씩 돌게 된다.
}


//String strVal = String.valueOf(scale.get_units(),1);


