#include "HX711.h" //HX711로드셀 라이브러리 불러오기
#define calibration_factor -7050.0 // 로드셀 초기값을 설정해줍니다. 이렇게 해주어야 처음에 작동시에 0점을 맞추는 것이라고 생각하시면 됩니다.
#define DOUT  3 //엠프 데이터 아웃 핀 
#define CLK  2  //엠프 클락  
HX711 scale(DOUT, CLK); //엠프 핀 선언 

void setup() {
  Serial.begin(115200); 

//  connect_wifi();
  Serial.println("Weight detection start");  
  scale.set_scale(calibration_factor);  
  scale.tare();   
}

void loop() {
  Serial.print("weight: ");
  Serial.print(scale.get_units(), 1);   
  Serial.print(" lbs"); //단위
  Serial.println(); 

//  server_post();
  
  delay(1000);
}


//String strVal = String.valueOf(scale.get_units(),1);


