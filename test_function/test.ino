#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include "HX711.h"
#define DOUT 3
#define CLK 2

HX711 scale(DOUT,CLK);
float calibration_factor = -80000;

const char *ssid = "InfoSec";
const char *password = "wjdqhqhgh16";

const char* host = "203.250.148.89";

int value = 0;
float w;

void setup() {
  Serial.begin(115200);
  scale.set_scale();
  scale.tare();
  delay(10);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

float getWeight(){
  float w = scale.get_units();
  if(w <=0){
    w = 0.0;
  }
  Serial.print(w,1);
  Serial.println(" kg");
  return(w);
}

void loop() {
  delay(5000);
  ++value;

  Serial.print("connecting to ");
  Serial.println(host);
  
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 7579;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  w = getWeight();
  delay(1000);
  
  
  // We now create a URI for the request
  String url = "/Mobius/Team_2/user";
//  url += streamId;
//  url += "?private_key=";
//  url += privateKey;
//  url += "&value=";
//  url += value;
  
  Serial.print("Requesting URL: ");
  Serial.println(url);
  String PostData = "{\"m2m:cin\": {\"con\": \"129\" }}";
  // This will send the request to the server
  client.println("POST /Mobius/Team_2/user HTTP/1.1");
  client.println("Accept: application/json");
  client.println("X-M2M-RI: 12345");
  client.println("X-M2M-Origin: SSJmmDDV65D");
  client.println("Postman-Token: a4e39dae-57b3-4c5a-91ff-e162dc6f25b4");
  client.println("Content-Type: application/vnd.onem2m-res+json; ty=4");
  client.println("Host: 203.250.148.89:7579");
  client.println("User-Agent: PostmanRuntime/7.26.8");
  client.println("Connection: keep-alive");
  client.print("Content-Length: ");
  client.println(PostData.length());
  client.println();
  client.println(PostData);
  int timeout = millis() + 5000;
  while (client.available() == 0) {
    if (timeout - millis() < 0) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
  
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  
  Serial.println();
  Serial.println("closing connection");
  return;
}
